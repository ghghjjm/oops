import biuoop.GUI;
import biuoop.DrawSurface;

import java.util.Random;
import java.awt.Color;

/**
 * class of AbstractArtDrawing, contains command functions. draw 10 random lines
 * and mark intersection and middle points
 *
 * @author Shachar & Yehoshua.
 *
 */
public class AbstractArtDrawing {
    /**
     * create AbstractArtDrawing object and call drawRandomLines function.
     *
     * @param args
     *            empty array
     */
    public static void main(String[] args) {
        AbstractArtDrawing abstractDraw = new AbstractArtDrawing();
        abstractDraw.drawRandomLines();
    }

    /**
     * mark the middle points in blue.
     *
     * @param middle
     *            the middle point of the line
     * @param d
     *            a DrawSurface object
     */
    public void drawBlueMiddle(Point middle, DrawSurface d) {
        //draw the middle point in blue color.
        d.setColor(Color.BLUE);
        d.fillCircle((int) middle.getX(), (int) middle.getY(), 2);
    }

    /**
     * mark the intersection points in red.
     *
     * @param intersection
     *            the intersection points of the two lines
     * @param d
     *            a DrawSurface object
     */
    public void drawRedIntercect(Point intersection, DrawSurface d) {
      //draw the middle point in red color.
        d.setColor(Color.RED);
        d.fillCircle((int) intersection.getX(), (int) intersection.getY(), 2);
    }

    /**
     * draw 10 random lines and mark intersection and middle points.
     */
    public void drawRandomLines() {
        //create an array of lines
        Line[] lineArray = new Line[10];
        //create surface
        GUI gui = new GUI("Random Lines Example", 600, 600);
        DrawSurface d = gui.getDrawSurface();
        //create the 10 lines.
        for (int i = 0; i < 10; ++i) {
            lineArray[i] = generateRandomLine();
            //draw the lines and their middle points
            drawLine(lineArray[i], d);
            drawBlueMiddle(lineArray[i].middle(), d);
        }
        //draw every intersection point for every line
        for (int i = 0; i < lineArray.length; i++) {
            for (int j = 0; j < lineArray.length; j++) {
                if (i != j) {
                    Point intersectionPoint = lineArray[i].intersectionWith(lineArray[j]);
                    if (intersectionPoint != null) {
                        drawRedIntercect(intersectionPoint, d);
                    }
                }
            }
        }
        //show all the lines and middle and intersection points.
        gui.show(d);
    }

    /**
     * generates random line.
     *
     * @return the random line
     */
    public Line generateRandomLine() {
        //get 4 coordinates in random way
        Random r = new Random();
        double x1 = r.nextDouble() * 600;
        double y1 = r.nextDouble() * 600;
        double x2 = r.nextDouble() * 600;
        double y2 = r.nextDouble() * 600;
        //create 2 points
        Point p1 = new Point(x1, y1);
        Point p2 = new Point(x2, y2);
        //create line
        Line line = new Line(p1, p2);
        return line;
    }

    /**
     * draw black line.
     *
     * @param l
     *            a line
     * @param d
     *            a DrawSurface object
     */
    public void drawLine(Line l, DrawSurface d) {
      //draw a line in black color.
        d.setColor(Color.BLACK);
        d.drawLine((int) l.start().getX(), (int) l.start().getY(), (int) l.end().getX(), (int) l.end().getY());

    }
}
