/**
 * class of border. contains the four borders of one ball. has get function.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Border {
    // members
    private int rightBorder;
    private int leftBorder;
    private int upBorder;
    private int downBorder;

    /**
     * constructor of Border.
     *
     * @param leftBorder
     *            the left border of the ball
     * @param upBorder
     *            the up border of the ball
     * @param rightBorder
     *            the right border of the ball
     * @param downBorder
     *            the down border of the ball
     */
    public Border(int leftBorder, int upBorder, int rightBorder, int downBorder) {
        // set the 4 borders.
        this.rightBorder = rightBorder;
        this.leftBorder = leftBorder;
        this.upBorder = upBorder;
        this.downBorder = downBorder;
    }

    /**
     * @return the right border of the ball.
     */
    public int getRight() {
        return this.rightBorder;
    }

    /**
     * @return the left border of the ball.
     */
    public int getLeft() {
        return this.leftBorder;
    }

    /**
     * @return the up border of the ball.
     */
    public int getUp() {
        return this.upBorder;
    }

    /**
     * @return the down border of the ball.
     */
    public int getDown() {
        return this.downBorder;
    }
}
