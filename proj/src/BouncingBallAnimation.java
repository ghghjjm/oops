import biuoop.DrawSurface;
import biuoop.GUI;
/**
 * run one bouncing ball.
 * set velocity by using angle and speed.
 * @author Shachar & Yehoshua.
 *
 */
public class BouncingBallAnimation {
    /**
     * main, run one bouncing ball.
     * @param args empty array.
     */
    public static void main(String[] args) {
        //create a surface
        GUI gui = new GUI("Bouncing Ball", 600, 600);
        //create a sleeper
        biuoop.Sleeper sleeper = new biuoop.Sleeper();
        //create a border
        Border border = new Border(0, 0, 600, 600);
        //create a ball
        Ball ball = new Ball(50, 50, 10, java.awt.Color.BLACK, border);
        //set velocity by angle and speed
        Velocity v = Velocity.fromAngleAndSpeed(30, 9);
        ball.setVelocity(v);
        //run animation of bouncing ball by using a sleeper
        while (true) {
            ball.moveOneStep();
            DrawSurface d = gui.getDrawSurface();
            ball.drawOn(d);
            //show the new position of the ball
            gui.show(d);
            sleeper.sleepFor(50);
        }
    }
}
