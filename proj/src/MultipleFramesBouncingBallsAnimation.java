import java.awt.Color;
import java.util.Random;

import biuoop.DrawSurface;
import biuoop.GUI;

public class MultipleFramesBouncingBallsAnimation {
    public static void main(String[] args) {
        GUI gui = new GUI("Bouncing Ball", 700, 700);
        biuoop.Sleeper sleeper = new biuoop.Sleeper();
        Random r = new Random();
        double randX, randY;
        Border b1 = new Border(50, 50, 500, 500);
        Border b2 = new Border(450, 450, 600, 600);
        Ball[] ballsArray = new Ball[args.length];
        for (int i = 0; i < ballsArray.length; i++) {
            Color color = new Color(r.nextInt());
            if (i < ballsArray.length / 2) {
                randX = r.nextDouble() * (450 - 2 * Integer.parseInt(args[i])) + (50 + Integer.parseInt(args[i]));
                randY = r.nextDouble() * (450 - 2 * Integer.parseInt(args[i])) + (50 + Integer.parseInt(args[i]));
                Point p = new Point(randX, randY);
                ballsArray[i] = new Ball(p, Integer.parseInt(args[i]), color, b1);
            } else {
                randX = r.nextDouble() * (150 - 2 * Integer.parseInt(args[i])) + (450 + Integer.parseInt(args[i]));
                randY = r.nextDouble() * (150 - 2 * Integer.parseInt(args[i])) + (450 + Integer.parseInt(args[i]));
                Point p = new Point(randX, randY);
                ballsArray[i] = new Ball(p, Integer.parseInt(args[i]), color, b2);
            }
            Velocity v;
            if (ballsArray[i].getRadius() <= 50) {
                v = Velocity.fromAngleAndSpeed(r.nextInt(360), 51 - ballsArray[i].getRadius());
            } else {
                v = Velocity.fromAngleAndSpeed(r.nextInt(360), 1);
            }
            ballsArray[i].setVelocity(v);
        }
        while (true) {
            DrawSurface d = gui.getDrawSurface();
            d.setColor(Color.GRAY);
            d.drawRectangle(50, 50, 450, 450);
            d.setColor(Color.YELLOW);
            d.drawRectangle(450, 450, 150, 150);
            for (int i = 0; i < ballsArray.length; i++) {
                ballsArray[i].moveOneStep();
                ballsArray[i].drawOn(d);
            }
            gui.show(d);
            sleeper.sleepFor(50); // wait for 50 milliseconds.
        }
    }
}
