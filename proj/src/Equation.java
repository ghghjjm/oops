/**
 * class of Equation, contains constructor, query and command functions.
 *
 * @author Shachar & Yehoshua.
 */
public class Equation {
    // members
    private double slope;
    private double yIntercept;
    private double xConst;
    // check whether the equation has const x
    private boolean constEquation;

    /**
     * constructor of Equation. calculates the equation of the line.
     *
     * @param ln
     *            a line.
     */
    public Equation(Line ln) {
        // calculates the slope and y intercept of the equation
        // using try and catch for the case of equation with const x
        try {
            this.slope = Equation.calculateSlope(ln.start(), ln.end());
            this.yIntercept = Equation.calculateYIntercept(ln.start(), this.slope);
            // the equation does not have const x
            this.constEquation = false;
            // the line is vertical
        } catch (RuntimeException e) {
            this.xConst = ln.start().getX();
            // the equation have const x
            this.constEquation = true;
        }
    }

    /**
     * calculates the slope of the equation.
     *
     * @param a
     *            first point
     * @param b
     *            second point
     * @return the slope of the equation.
     * @throws RuntimeException
     *             if the double is infinite.
     */
    public static double calculateSlope(Point a, Point b) throws RuntimeException {
        // calculates the slope
        double slope = (a.getY() - b.getY()) / (a.getX() - b.getX());
        // if the line is vertical the slope is "infinity"
        if (Double.isInfinite(slope)) {
            // create a runtime exception for this case
            throw new RuntimeException("slope divided by zero");
        }
        return slope;
    }

    /**
     * calculate the y-intercept of the equation.
     *
     * @param a
     *            one point
     * @param slope
     *            the slope of the equation.
     * @return the y-intercept of the equation
     */
    public static double calculateYIntercept(Point a, double slope) {
        // calculate the y intercept
        double yIntercept = a.getY() - slope * a.getX();
        return yIntercept;
    }

    /**
     * calculate the y value of one x coordinate.
     *
     * @param x
     *            x coordinate
     * @return the y value of one x coordinate
     */
    public double calculateYValue(double x) {
        // calculate the y value according to the equation
        double yValue = this.slope * x + this.yIntercept;
        return yValue;
    }

    /**
     *
     * @return the slope of the equation.
     */
    public double getSlope() {
        return this.slope;
    }

    /**
     *
     * @return the y-intercept of the equation.
     */
    public double getYIntercept() {
        return this.yIntercept;
    }

    /**
     *
     * @return the constant X of the equation, if the equation has constant X
     *         value.
     */
    public double getXConst() {
        return this.xConst;
    }

    /**
     * check whether the equation has constant x value.
     *
     * @return whether the equation has constant x value.
     */
    public boolean isConstEquation() {
        return this.constEquation;
    }
}
