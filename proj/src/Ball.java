import biuoop.DrawSurface;

/**
 * class of ball. contains constructor and query and command functions. can draw
 * the ball and move it to new location.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Ball {
    //members
    private int radius;
    private Point center;
    private java.awt.Color color;
    private Velocity velocity;
    private Border border;

    /**
     * constructor of ball according to center point.
     *
     * @param center
     *            the point of the center of the ball.
     * @param r
     *            the radius of the ball.
     * @param color
     *            the color of the ball.
     * @param border
     *            a Border object
     */
    public Ball(Point center, int r, java.awt.Color color, Border border) {
        // set radius, center,color and border
        this.radius = r;
        this.center = center;
        this.color = color;
        this.border = border;
        // set default velocity
        this.velocity = new Velocity(0, 0);
    }

    /**
     * constructor of ball according to center coordinates.
     *
     * @param x
     *            the x coordinate of the center point.
     * @param y
     *            the y coordinate of the center point.
     * @param r
     *            the radius of the ball.
     * @param color
     *            the color of the ball.
     * @param border
     *            a Border object
     */
    public Ball(int x, int y, int r, java.awt.Color color, Border border) {
        // set radius, center,color and border
        this.radius = r;
        this.center = new Point(x, y);
        this.color = color;
        this.border = border;
        // set default velocity
        this.velocity = new Velocity(0, 0);
    }

    /**
     * @return the x coordinate of the center point.
     */
    public int getX() {
        return (int) this.center.getX();
    }

    /**
     * @return the y coordinate of the center point.
     */
    public int getY() {
        return (int) this.center.getY();
    }

    /**
     * @return the radius of the ball.
     */
    public int getRadius() {
        return this.radius;
    }

    /**
     * @return the color of the ball.
     */
    public java.awt.Color getColor() {
        return this.color;
    }

    /**
     * draw the ball.
     *
     * @param surface
     *            a DrawSurface object.
     */
    public void drawOn(DrawSurface surface) {
        // draw a ball
        surface.setColor(this.color);
        surface.fillCircle((int) this.center.getX(), (int) this.center.getY(), this.radius);
    }

    /**
     * set the ball's velocity by getting a Velocity variable.
     *
     * @param v
     *            the velocity of the ball.
     */
    public void setVelocity(Velocity v) {
        this.velocity = new Velocity(v.getDX(), v.getDY());
    }

    /**
     * set the ball's velocity by getting a dx and dy.
     *
     * @param dx
     *            the dx of the velocity
     * @param dy
     *            the dx of the velocity
     */
    public void setVelocity(double dx, double dy) {
        this.velocity = new Velocity(dx, dy);
    }

    /**
     * @return the velocity of the ball.
     */
    public Velocity getVelocity() {
        return this.velocity;
    }

    /**
     * determines the velocity of the ball according to its location and border
     * and then move the ball's center to next location.
     */
    public void moveOneStep() {
        // if the next move will be out of the down border change the velocity
        // to the opposite side
        if ((this.center.getY() + this.radius + this.velocity.getDY() >= this.border.getDown())) {
            this.setVelocity(this.velocity.getDX(), -this.velocity.getDY());
        }
        // if the next move will be out of the right border change the velocity
        // to the opposite side
        if ((this.center.getX() + this.radius + this.velocity.getDX() >= this.border.getRight())) {
            this.setVelocity(-this.velocity.getDX(), this.velocity.getDY());
        }
        // if the next move will be out of the left border change the velocity
        // to the opposite side
        if (this.center.getX() - this.radius + this.velocity.getDX() <= this.border.getLeft()) {
            this.setVelocity(Math.abs(this.velocity.getDX()), this.velocity.getDY());
        }
        // if the next move will be out of the up border change the velocity to
        // the opposite side
        if (this.center.getY() - this.radius + this.velocity.getDY() <= this.border.getUp()) {
            this.setVelocity(this.velocity.getDX(), Math.abs(this.velocity.getDY()));
        }
        // move the center point to new location
        this.center = this.getVelocity().applyToPoint(this.center);
    }
}
