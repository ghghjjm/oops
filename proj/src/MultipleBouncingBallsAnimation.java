import java.util.Random;

import biuoop.DrawSurface;
import biuoop.GUI;

import java.awt.Color;

public class MultipleBouncingBallsAnimation {
    public static void main(String[] args) {
        GUI gui = new GUI("Bouncing Ball", 600, 600);
        biuoop.Sleeper sleeper = new biuoop.Sleeper();
        Random r = new Random();
        double randX, randY;
        Border b = new Border(0, 0, 600, 600);
        Ball[] ballsArray = new Ball[args.length];
        for (int i = 0; i < ballsArray.length; i++) {
            randX = r.nextDouble() * 600;
            randY = r.nextDouble() * 600;
            Color color = new Color(r.nextInt());
            Point p = new Point(randX, randY);
            ballsArray[i] = new Ball(p, Integer.parseInt(args[i]), color, b);
            Velocity v;
            if (ballsArray[i].getRadius() <= 50) {
                v = Velocity.fromAngleAndSpeed(r.nextInt(360), 51 - ballsArray[i].getRadius());
            } else {
                v = Velocity.fromAngleAndSpeed(r.nextInt(360), 1);
            }
            ballsArray[i].setVelocity(v);
        }
        while (true) {
            DrawSurface d = gui.getDrawSurface();
            for (int i = 0; i < ballsArray.length; i++) {
                ballsArray[i].moveOneStep();
                ballsArray[i].drawOn(d);
            }
            gui.show(d);
            sleeper.sleepFor(50); // wait for 50 milliseconds.
        }
    }
}
