/**
 * @author Shachar sirotkin.
 * version 1.7
 * @since 03-03-2016
 */
public class DescribeNumbers {
    /**
     * print the min, max and average.
     *
     * @param args
     *            Array contains strings of numbers.
     */
    public static void main(String[] args) {
        // no args is wrong input
        if (args.length == 0) {
            System.out.println("sorry,Wrong input");
            return;
        }
        // convert args to integers
        int[] convertedArgs = stringsToInts(args);
        System.out.println("min: " + min(convertedArgs));
        System.out.println("max: " + max(convertedArgs));
        System.out.println("avg: " + avg(convertedArgs));
    }

    /**
     * convert the array of strings to array of integers.
     *
     * @param numbers
     *            Array of strings of numbers.
     * @return array of the converted integers.
     */
    public static int[] stringsToInts(String[] numbers) {
        int[] convertedArray = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            convertedArray[i] = Integer.parseInt(numbers[i]);
        }
        return convertedArray;
    }

    /**
     * find the minimal number.
     *
     * @param numbers
     *            Array contains strings of numbers.
     * @return the minimal number.
     */
    public static int min(int[] numbers) {
        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (min > numbers[i]) {
                min = numbers[i];
            }
        }
        return min;
    }

    /**
     * find the maximal number.
     *
     * @param numbers
     *            Array of numbers.
     * @return the maximal number.
     */
    public static int max(int[] numbers) {
        int max = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (max < numbers[i]) {
                max = numbers[i];
            }
        }
        return max;
    }

    /**
     * find the average of the numbers.
     *
     * @param numbers
     *            Array contains strings of numbers.
     * @return the average of the numbers.
     */
    public static float avg(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return (float) sum / numbers.length;
    }
}